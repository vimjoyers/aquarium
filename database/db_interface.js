const {Pool} = require('pg');

const db_connection = new Pool({
    user: process.env.POSTGRES_USER,
    host: 'db',
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.DB_NAME,
    port: 5432
});

class DatabaseFunctions {
  constructor(){
    const client = db_connection.connect();
  }

  CreateTable = async() => {
    try{
      await db_connection.query(`CREATE TABLE IF NOT EXISTS Aquarium (
        ID SERIAL PRIMARY KEY,
        Name varchar(255) NOT NULL,
        Kind varchar(255) NOT NULL,
        Age INT DEFAULT 0,
        Food INT DEFAULT 60,
        LifeLevel INT DEFAULT 100
      );`);
    } catch(error) {
      console.error('Error fetching data from PostgreSQL:', error);
    }
  }

  SeeAllData = async() => {
    try{  
     const response = await db_connection.query('SELECT * FROM Aquarium ORDER BY id');
     return response;
    } 
    catch (error) {
      console.error('Error fetching data from PostgreSQL:', error);
    };
  }

  GetFeedLevel = async() => {
    try{
      const feedResponse = await db_connection.query('SELECT food FROM aquarium;')
      return feedResponse;
    }
    catch(error){
      console.log("Can't get data about feed level");
      throw error;
    }
  }

  Cleanliness = async() =>{
    try{
      const cleanlinessLevel = await db_connection.query('SELECT cleanliness FROM aquarium;');
      return cleanlinessLevel;
    }
    catch(error){
      console.log("Can't get data about cleanliness level");
      throw error;
    }
  }
}

module.exports = DatabaseFunctions, db_connection;
