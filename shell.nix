let
   pkgs = import <nixpkgs> {
    config = {
      allow_insecure = true;
    };
   };
  in
   pkgs.mkShell {
    AWS_PROFILE = "personal";
    packages = with pkgs; [
      nodejs_20 
      yarn
    ];
   }
  
