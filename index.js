const express = require('express');
const path = require('path');
const DatabaseFunctions = require('./database/db_interface');
const { assert } = require('console');
const app = express();
const port = 8001;
var db = new DatabaseFunctions;  

app.use(express.static("static"))
app.use(express.json())

app.get('/', (req, res) => {   
  res.sendFile(path.join(__dirname, 'static/index.html')); 
})  

db.CreateTable();  
let userInput = {
  clean: false,
  feed: false
}
app.get('/view', async (req, res) => {   
  console.log('/view')
  const data = await db.SeeAllData();
  res.json(data); 
});  


app.get('/userinput', (req, res) => {
  console.log('GET /userinput')
  console.log(userInput)
  res.json(userInput)
  userInput.clean = false
  userInput.feed = false
})
app.post('/userinput', (req, res) => {
  console.log('POST /userinput')
  console.log(req.body)
   userInput.clean = req.body.clean ? true : false
   userInput.feed = req.body.feed ? true : false
   res.status(200)
})
app.get('/food', async (req,res) =>{
  const getfood = await db.GetFeedLevel();
  res.json(getfood);
});


app.listen(port, () => {   
  console.log(`Example app listening on port ${port}`) }) 
