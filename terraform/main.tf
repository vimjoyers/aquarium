provider "aws" {
  region = "eu-central-1"
}


resource "aws_instance" "web" {
  ami                    = "ami-0f7204385566b32d0"
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.web-sg.id]
  key_name               = aws_key_pair.kp.key_name

  tags = {
    Name = "aquarium"
  }
}
resource "aws_instance" "runner" {
  ami                    = "ami-0f7204385566b32d0"
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.web-sg.id]
  key_name               = aws_key_pair.kp.key_name

  tags = {
    Name = "runner"
  }
}



resource "aws_key_pair" "kp" {
  key_name   = "aquarium-key"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINBISdjCHXpi9H6+Ny8toHIu04sBGC6XrMQEFnUtbkWy csyankovskyi@nixos"
}

resource "aws_security_group" "web-sg" {
  name = "aquarium-sg"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 }
